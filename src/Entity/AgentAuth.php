<?php

namespace App\Entity;

use App\Repository\AgentAuthRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AgentAuthRepository::class)]
class AgentAuth
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $gestionnaireId;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $isAuth;

    #[ORM\Column(type: 'string', length: 255)]
    private $password;

    #[ORM\Column(type: 'date', nullable: true)]
    private $authDate;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $isValid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGestionnaireId(): ?int
    {
        return $this->gestionnaireId;
    }

    public function setGestionnaireId(int $gestionnaireId): self
    {
        $this->gestionnaireId = $gestionnaireId;

        return $this;
    }

    public function getIsAuth(): ?bool
    {
        return $this->isAuth;
    }

    public function setIsAuth(?bool $isAuth): self
    {
        $this->isAuth = $isAuth;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getAuthDate(): ?\DateTimeInterface
    {
        return $this->authDate;
    }

    public function setAuthDate(?\DateTimeInterface $authDate): self
    {
        $this->authDate = $authDate;

        return $this;
    }

    public function getIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(?bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }
}
