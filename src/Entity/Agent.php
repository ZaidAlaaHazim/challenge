<?php

namespace App\Entity;

use App\Repository\AgentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AgentRepository::class)]
class Agent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $gestionnaireId;

    #[ORM\Column(type: 'string', length: 255)]
    private $libelle;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $vipMoto;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $vipCollection;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $vipPlaisance;

    #[ORM\Column(type: 'integer')]
    private $poule_id;

    #[ORM\Column(type: 'array')]
    private $mail = [];

    #[ORM\Column(type: 'integer', nullable: true)]
    private $employeMotoId;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $employeCollectionId;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $employePlaisanceId;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $agentAuth;

    #[ORM\ManyToMany(targetEntity: Course::class, mappedBy: 'gestionnaireId')]
    private $courses;

    public function __construct()
    {
        $this->courses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGestionnaireId(): ?int
    {
        return $this->gestionnaireId;
    }

    public function setGestionnaireId(int $gestionnaireId): self
    {
        $this->gestionnaireId = $gestionnaireId;

        return $this;
    }

    public function getAgentAuth(): ?int
    {
        return $this->agentAuth;
    }

    public function setAgentAuth(int $agentAuth): self
    {
        $this->agentAuth = $agentAuth;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getVipMoto(): ?string
    {
        return $this->vipMoto;
    }

    public function setVipMoto(?string $vipMoto): self
    {
        $this->vipMoto = $vipMoto;

        return $this;
    }

    public function getVipCollection(): ?string
    {
        return $this->vipCollection;
    }

    public function setVipCollection(?string $vipCollection): self
    {
        $this->vipCollection = $vipCollection;

        return $this;
    }

    public function getVipPlaisance(): ?string
    {
        return $this->vipPlaisance;
    }

    public function setVipPlaisance(?string $vipPlaisance): self
    {
        $this->vipPlaisance = $vipPlaisance;

        return $this;
    }

    public function getPouleId(): ?int
    {
        return $this->poule_id;
    }

    public function setPouleId(int $poule_id): self
    {
        $this->poule_id = $poule_id;

        return $this;
    }

    public function getMail(): ?array
    {
        return $this->mail;
    }

    public function setMail(array $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getEmployeMotoId(): ?int
    {
        return $this->employeMotoId;
    }

    public function setEmployeMotoId(?int $employeMotoId): self
    {
        $this->employeMotoId = $employeMotoId;

        return $this;
    }

    public function getEmployeCollectionId(): ?int
    {
        return $this->employeCollectionId;
    }

    public function setEmployeCollectionId(?int $employeCollectionId): self
    {
        $this->employeCollectionId = $employeCollectionId;

        return $this;
    }

    public function getEmployePlaisanceId(): ?int
    {
        return $this->employePlaisanceId;
    }

    public function setEmployePlaisanceId(?int $employePlaisanceId): self
    {
        $this->employePlaisanceId = $employePlaisanceId;

        return $this;
    }



    /**
     * @return Collection|Course[]
     */
    public function getCourses(): Collection
    {
        return $this->courses;
    }

    public function addCourse(Course $course): self
    {
        if (!$this->courses->contains($course)) {
            $this->courses[] = $course;
            $course->addGestionnaireId($this);
        }

        return $this;
    }

    public function removeCourse(Course $course): self
    {
        if ($this->courses->removeElement($course)) {
            $course->removeGestionnaireId($this);
        }

        return $this;
    }
}
