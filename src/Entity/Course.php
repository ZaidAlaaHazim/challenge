<?php

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CourseRepository::class)]
class Course
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToMany(targetEntity: Agent::class, inversedBy: 'courses')]
    private $gestionnaireId;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANMoto;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANMotoSdce;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANMotoF4;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANCollection;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANCollectionPassion;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANCollectionF3;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANPlaisanceF1;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANPlaisanceF1Sem;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANPlaisanceF2F3;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $ANCampingCar;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $APNMoto;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $APNCollection;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $APNPlaisance;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $APNCampingCar;

    public function __construct()
    {
        $this->gestionnaireId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Agent[]
     */
    public function getGestionnaireId(): Collection
    {
        return $this->gestionnaireId;
    }

    public function addGestionnaireId(Agent $gestionnaireId): self
    {
        if (!$this->gestionnaireId->contains($gestionnaireId)) {
            $this->gestionnaireId[] = $gestionnaireId;
        }

        return $this;
    }

    public function removeGestionnaireId(Agent $gestionnaireId): self
    {
        $this->gestionnaireId->removeElement($gestionnaireId);

        return $this;
    }

    public function getANMoto(): ?int
    {
        return $this->ANMoto;
    }

    public function setANMoto(?int $ANMoto): self
    {
        $this->ANMoto = $ANMoto;

        return $this;
    }

    public function getANMotoSdce(): ?int
    {
        return $this->ANMotoSdce;
    }

    public function setANMotoSdce(?int $ANMotoSdce): self
    {
        $this->ANMotoSdce = $ANMotoSdce;

        return $this;
    }

    public function getANMotoF4(): ?int
    {
        return $this->ANMotoF4;
    }

    public function setANMotoF4(?int $ANMotoF4): self
    {
        $this->ANMotoF4 = $ANMotoF4;

        return $this;
    }

    public function getANCollection(): ?int
    {
        return $this->ANCollection;
    }

    public function setANCollection(?int $ANCollection): self
    {
        $this->ANCollection = $ANCollection;

        return $this;
    }

    public function getANCollectionPassion(): ?int
    {
        return $this->ANCollectionPassion;
    }

    public function setANCollectionPassion(?int $ANCollectionPassion): self
    {
        $this->ANCollectionPassion = $ANCollectionPassion;

        return $this;
    }

    public function getANCollectionF3(): ?int
    {
        return $this->ANCollectionF3;
    }

    public function setANCollectionF3(?int $ANCollectionF3): self
    {
        $this->ANCollectionF3 = $ANCollectionF3;

        return $this;
    }

    public function getANPlaisanceF1(): ?int
    {
        return $this->ANPlaisanceF1;
    }

    public function setANPlaisanceF1(?int $ANPlaisanceF1): self
    {
        $this->ANPlaisanceF1 = $ANPlaisanceF1;

        return $this;
    }

    public function getANPlaisanceF1Sem(): ?int
    {
        return $this->ANPlaisanceF1Sem;
    }

    public function setANPlaisanceF1Sem(?int $ANPlaisanceF1Sem): self
    {
        $this->ANPlaisanceF1Sem = $ANPlaisanceF1Sem;

        return $this;
    }

    public function getANPlaisanceF2F3(): ?int
    {
        return $this->ANPlaisanceF2F3;
    }

    public function setANPlaisanceF2F3(?int $ANPlaisanceF2F3): self
    {
        $this->ANPlaisanceF2F3 = $ANPlaisanceF2F3;

        return $this;
    }

    public function getANCampingCar(): ?int
    {
        return $this->ANCampingCar;
    }

    public function setANCampingCar(?int $ANCampingCar): self
    {
        $this->ANCampingCar = $ANCampingCar;

        return $this;
    }

    public function getAPNMoto(): ?int
    {
        return $this->APNMoto;
    }

    public function setAPNMoto(?int $APNMoto): self
    {
        $this->APNMoto = $APNMoto;

        return $this;
    }

    public function getAPNCollection(): ?int
    {
        return $this->APNCollection;
    }

    public function setAPNCollection(?int $APNCollection): self
    {
        $this->APNCollection = $APNCollection;

        return $this;
    }

    public function getAPNPlaisance(): ?int
    {
        return $this->APNPlaisance;
    }

    public function setAPNPlaisance(?int $APNPlaisance): self
    {
        $this->APNPlaisance = $APNPlaisance;

        return $this;
    }

    public function getAPNCampingCar(): ?int
    {
        return $this->APNCampingCar;
    }

    public function setAPNCampingCar(?int $APNCampingCar): self
    {
        $this->APNCampingCar = $APNCampingCar;

        return $this;
    }
}
