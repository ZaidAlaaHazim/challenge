<?php

namespace App\Repository;

use App\Entity\AgentAuth;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AgentAuth|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgentAuth|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgentAuth[]    findAll()
 * @method AgentAuth[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgentAuthRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgentAuth::class);
    }

    // /**
    //  * @return AgentAuth[] Returns an array of AgentAuth objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgentAuth
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
