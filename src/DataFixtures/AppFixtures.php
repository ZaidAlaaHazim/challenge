<?php

namespace App\DataFixtures;

use App\Entity\Agent;
use App\Entity\AgentAuth;
use App\Entity\Employe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\ORM\Doctrine\Populator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Faker;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture implements ContainerAwareInterface, OrderedFixtureInterface
{

    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $objectManager)
    {
        // initialisation de l'objet Faker
        // on peut préciser en paramètre la localisation,
        // pour avoir des données qui semblent "françaises"
        $faker = Faker\Factory::create('fr_FR');

        $agentRepo = $objectManager->getRepository(Agent::class);
        $agents = Array();
        $agents_auth = Array();
        for ($i = 0; $i < 10; $i++) {
            $agents[$i] = new Agent();
            $agents[$i]->setGestionnaireId($faker->numberBetween(0, 1000));
            $agents[$i]->setPouleId($faker->numberBetween(1, 4));
            $agents[$i]->setMail(array($faker->email));
            $agents[$i]->setLibelle($faker->name);

            $agents_auth[$i] = new AgentAuth();
            $agents_auth[$i]->setGestionnaireId($agents[$i]->getGestionnaireId());
            $agents_auth[$i]->setIsAuth(0);
            $agents_auth[$i]->setIsValid(0);
            $agents_auth[$i]->setPassword("azerty");

            $objectManager->persist($agents[$i]);
            $objectManager->persist($agents_auth[$i]);
        }
        $objectManager->flush();

        for ($j = 0; $j < 10; $j++){
            $agents[$j]->setAgentAuth($agents_auth[$j]->getId());

            $objectManager->persist($agents[$j]);
            $objectManager->persist($agents_auth[$j]);
        }

        $objectManager->flush();
        // le deuxième paramètre (10) correspond au nombre d'objets qui vont être créés
    }

    public function getOrder()
    {
        return 1;
    }

}